﻿using TechTalk.SpecFlow;

namespace Embarr.SpecFlow.Services
{
    /// <summary>
    /// Service for accessing the SpecFlow FeatureContext
    /// </summary>
    public class FeatureContextService : ContextService
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FeatureContextService"/> class.
        /// </summary>
        public FeatureContextService()
            : base(FeatureContext.Current)
        {
            
        }
    }
}
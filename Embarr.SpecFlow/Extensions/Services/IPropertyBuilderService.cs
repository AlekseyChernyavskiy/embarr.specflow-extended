using Embarr.SpecFlow.Extensions.Models;

namespace Embarr.SpecFlow.Extensions.Services
{
    public interface IPropertyBuilderService
    {
        void BuildProperty(Property property, int partIndex, object parent);
    }
}
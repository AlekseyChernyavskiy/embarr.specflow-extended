﻿using System;

namespace Embarr.SpecFlow.Extensions.Services
{
    public class SimpleTypeService : ISimpleTypeService
    {
        public object GetSimpleTypeValue(string value, Type type)
        {
            if (type == typeof(decimal) || type == typeof(decimal?))
            {
                return decimal.Parse(value);
            }

            if (type == typeof(double) || type == typeof(double?))
            {
                return double.Parse(value);
            }

            if (type == typeof(float) || type == typeof(float?))
            {
                return float.Parse(value);
            }

            if (type == typeof(int) || type == typeof(int?))
            {
                return int.Parse(value);
            }

            if (type == typeof(Int64) || type == typeof(Int64?))
            {
                return Int64.Parse(value);
            }

            if (type == typeof(UInt64) || type == typeof(UInt64?))
            {
                return UInt64.Parse(value);
            }

            if (type == typeof(Int32) || type == typeof(Int32?))
            {
                return Int32.Parse(value);
            }

            if (type == typeof(UInt32) || type == typeof(UInt32?))
            {
                return UInt32.Parse(value);
            }

            if (type == typeof(Int16) || type == typeof(Int16?))
            {
                return Int16.Parse(value);
            }

            if (type == typeof(UInt16) || type == typeof(UInt16?))
            {
                return UInt16.Parse(value);
            }

            if (type == typeof(short) || type == typeof(short?))
            {
                return short.Parse(value);
            }

            if (type == typeof(Byte) || type == typeof(Byte?))
            {
                return Byte.Parse(value);
            }

            if (type == typeof(SByte) || type == typeof(SByte?))
            {
                return SByte.Parse(value);
            }

            if (type == typeof(bool) || type == typeof(bool?))
            {
                return bool.Parse(value);
            }

            if (type == typeof(DateTime) || type == typeof(DateTime?))
            {
                return DateTime.Parse(value);
            }

            if (type == typeof(char) || type == typeof(char?))
            {
                return char.Parse(value);
            }

            return value;
        }
    }
}
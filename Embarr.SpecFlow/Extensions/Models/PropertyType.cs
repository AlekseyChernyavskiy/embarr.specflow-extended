﻿namespace Embarr.SpecFlow.Extensions.Models
{
    public enum PropertyType
    {
        Standard,
        KeyedOrIndexer
    }
}
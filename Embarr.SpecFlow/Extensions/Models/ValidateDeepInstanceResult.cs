﻿using System.Collections.Generic;

namespace Embarr.SpecFlow.Extensions.Models
{
    public class ValidateDeepInstanceResult
    {
        public ValidateDeepInstanceResult()
        {
            Properties = new List<ValidateDeepInstanceProperty>();
            SuccessfulMatches = new List<ValidateDeepInstanceProperty>();
            FailedMatches = new List<ValidateDeepInstanceProperty>();
        }

        public List<ValidateDeepInstanceProperty> Properties { get; private set; }

        public List<ValidateDeepInstanceProperty> SuccessfulMatches { get; private set; }

        public List<ValidateDeepInstanceProperty> FailedMatches { get; private set; }

        public bool AllMatch
        {
            get { return FailedMatches.Count == 0; }
        }
    }
}
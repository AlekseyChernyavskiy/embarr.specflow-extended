﻿using System.Collections.Generic;

namespace Embarr.SpecFlow.UnitTests.Models
{
    public class TestModel
    {
        public string IamReadOnly
        {
            get { return "ReadOnlyString"; }
        }

        public string SimpleStringType { get; set; }

        public TestModel Child { get; set; }

        public string[] SimpleTypeArray { get; set; }

        public TestModel[] ComplexTypeArray { get; set; }
     
        public List<string> SimpleTypeList { get; set; }

        public List<TestModel> ComplexTypeList { get; set; }

        public Dictionary<string, string> SimpleTypeDictionary { get; set; }

        public Dictionary<string, TestModel> ComplexTypeDictionary { get; set; } 

        public Dictionary<int, TestModel> ComplexTypeIntKeyedDictionary { get; set; } 
    }
}
